package cm.nn.revolut_app.State;


import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import cm.nn.core.io.CurrencyPopulater;
import cm.nn.revolut_app.Adapter.CurrencyAdapter;

public class OnTextChange implements EditText.OnEditorActionListener {

    private final String key;
    private final CurrencyAdapter.OnCurrencyEdit editor;

    public OnTextChange(String _key, CurrencyAdapter.OnCurrencyEdit _editer) {
        this.key = _key;
        this.editor = _editer;
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_DONE && key.contentEquals(CurrencyPopulater.getBASE().getKey())) {
            editor.onEdit(v.getText().toString());
            return true;
        }
        return false;
    }
}
