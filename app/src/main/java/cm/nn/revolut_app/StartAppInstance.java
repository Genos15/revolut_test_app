package cm.nn.revolut_app;

import android.app.Application;

import cm.nn.core.TimeManager.AlarmReceiver;

public class StartAppInstance extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        AlarmReceiver.setAlarm(this, true);
    }

    @Override
    public void onTerminate() {
        AlarmReceiver.cancelAlarm(this);
        super.onTerminate();
    }

    @Override
    public void onLowMemory() {
        AlarmReceiver.cancelAlarm(this);
        super.onLowMemory();
    }

}
