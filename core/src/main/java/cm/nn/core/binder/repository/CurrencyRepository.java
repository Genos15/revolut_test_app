package cm.nn.core.binder.repository;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;


import cm.nn.core.binder.model.Currency;

public enum CurrencyRepository {

    instance;

    private MutableLiveData<Currency> currency;

    {
        currency = new MutableLiveData<>();
    }

    public void setData(@NonNull final Currency _currency) {
        currency.postValue(_currency);
    }

    public LiveData<Currency> getData() {
        return currency;
    }

}
