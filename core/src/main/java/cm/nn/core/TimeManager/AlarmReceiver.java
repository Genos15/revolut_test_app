package cm.nn.core.TimeManager;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;

import cm.nn.core.BuildConfig;
import cm.nn.core.Source.PureIntentService;
import io.reactivex.annotations.NonNull;

public class AlarmReceiver extends BroadcastReceiver {

    private static final long TIME_AFTER = 1L;

    @Override
    public void onReceive(Context context, Intent intent) {
        if (isOnline(context)) {
            PureIntentService.enqueueWork(context, intent);
        }
    }

    public static void cancelAlarm(@NonNull Context context) {
        AlarmManager alarm = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        if (alarm == null) return;
        alarm.cancel(getPendingIntent(context));
    }

    public static void setAlarm(@NonNull Context context, boolean force) {
        cancelAlarm(context);
        AlarmManager alarm = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        if (alarm == null) return;
        long delay = (1000L * 60L * TIME_AFTER);
        long when = System.currentTimeMillis();
        if (!force) {
            when += delay;
        }
        alarm.set(AlarmManager.RTC_WAKEUP, when, getPendingIntent(context));
    }

    private static PendingIntent getPendingIntent(Context context) {
        Intent alarmIntent = new Intent(context, AlarmReceiver.class);
        alarmIntent.setAction(BuildConfig.ALARM_INTENT);
        return PendingIntent.getBroadcast(context, 0, alarmIntent, PendingIntent.FLAG_CANCEL_CURRENT);
    }

    private boolean isOnline(@NonNull Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm != null && cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnectedOrConnecting();
    }
}
