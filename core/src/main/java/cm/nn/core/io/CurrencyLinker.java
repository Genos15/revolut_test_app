package cm.nn.core.io;

import cm.nn.core.binder.model.Currency;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface CurrencyLinker {

    @GET("/latest")
    Observable<Currency> GET_CURRENCIES(@Query("base") String base);

}
