package cm.nn.core.Source;

import android.content.Context;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.core.app.JobIntentService;

import cm.nn.core.TimeManager.AlarmReceiver;
import cm.nn.core.io.CurrencyPopulater;

public class PureIntentService extends JobIntentService {

    private static final int JOB_ID = 1502;

    public static void enqueueWork(Context context, Intent intent) {
        enqueueWork(context, PureIntentService.class, JOB_ID, intent);
    }

    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        CurrencyPopulater.Populating();
        AlarmReceiver.setAlarm(getApplicationContext(), false);
        stopSelf();
    }

}
